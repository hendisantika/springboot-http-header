package com.hendisantika.springboothttpheader.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-http-header
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 12/02/20
 * Time: 07.35
 */
@RestController
public class IndividualHeaderController {

    private static final Logger LOG = LoggerFactory.getLogger(IndividualHeaderController.class);

    @GetMapping("/products/product/{code}")
    public String getProduct(@PathVariable("code") String code, @RequestHeader("accept-language") String language) {
        //logic based on the header value
        LOG.info("Accepted language is {}", language);
        return "DemoProduct";
    }
}
