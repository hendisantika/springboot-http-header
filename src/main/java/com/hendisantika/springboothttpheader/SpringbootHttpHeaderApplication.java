package com.hendisantika.springboothttpheader;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootHttpHeaderApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootHttpHeaderApplication.class, args);
    }

}
